# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This repo is for any and all brightpeak asset ui testing.
It is built with gradle, java, junit, and selenium.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Ensure you have JDK and Gradle setup appropriately along with your IDE.

* Configuration
* Dependencies
* Database configuration
* How to run tests

You just type "./gradlew clean test" on the command line

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Owner is nick.hammer@brightpeakfinancial.com

* Other community or team contact