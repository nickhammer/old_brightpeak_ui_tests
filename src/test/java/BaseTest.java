import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;

public class BaseTest {

    public static WebDriver driver;

    @BeforeClass
    public static void setUp(){

        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win")) {
            System.setProperty("webdriver.gecko.driver","driver" + File.separator + "geckodriver.exe");
        } else {
            System.setProperty("webdriver.gecko.driver","driver" + File.separator + "geckodriver");

        }

        if (os.contains("win")) {
            System.setProperty("webdriver.chrome.driver", "driver" + File.separator + "chromedriver.exe");
        } else {
            System.setProperty("webdriver.chrome.driver", "driver" + File.separator + "chromedriver");
        }

        driver = new ChromeDriver();
        //driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
        System.out.println("Browser is closed");
    }

    public static void pauseTest(int secs) {
        try {
            Thread.sleep(1000L * (long)secs);
        } catch (InterruptedException var2) {
            // do nothing
        }

    }
}
