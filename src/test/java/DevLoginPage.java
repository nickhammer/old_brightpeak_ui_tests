
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;

import static org.junit.Assert.*;

public class DevLoginPage extends BaseTest{

    @Test
    public void testLoginPage() {

        /*
        TODO: Execute this only if we are doing Chrome and on a Mac
         */

//        // Mac specific resizing of the Chrome window because it was too small
//        System.out.println(driver.manage().window().getSize());
//        Dimension d = new Dimension(1500,1000);
//        //Resize the current window to the given dimension
//        driver.manage().window().setSize(d);

        driver.get("https://dh.dev.brightpeakfinancial.com/engage/dashboard");
        WebElement loginLink = driver.findElement(By.linkText("Login"));
        loginLink.click();
        assertEquals(driver.getCurrentUrl(),"https://dh.dev.brightpeakfinancial.com/engage/account/login");
    }
}