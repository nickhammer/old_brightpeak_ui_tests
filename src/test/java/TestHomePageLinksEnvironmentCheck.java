import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;


public class TestHomePageLinksEnvironmentCheck extends BaseTest {

    private String url;

    @Test
    public void testTheEnvironments(){
        testTheHomePage("https://test.brightpeakfinancial.com/");
        testTheHomePage("https://dev.brightpeakfinancial.com/");
        testTheHomePage("https://www.brightpeakfinancial.com/");
    }


    public void testTheHomePage(String url){
        driver.get(url);
        pauseTest(4);
        if (driver.manage().window().getSize().getWidth() < 1200) {
            WebElement threeBarMenu = driver.findElement(By.xpath("//*[@id=\"mk-header-1\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]"));
            threeBarMenu.click();
            // None of the tests work because of DOM changes done by Angular as you click
            // Trying to get the focus, but for now these are commented out
//            assert(driver.findElement(By.linkText("Get Started")).isDisplayed());
//            assert(driver.findElement(By.linkText("Products")).isDisplayed());
//            assert(driver.findElement(By.linkText("Resources")).isDisplayed());
//            assert(driver.findElement(By.linkText("About")).isDisplayed());
//            assert(driver.findElement(By.linkText("Tips & Stories")).isDisplayed());
//            assert(driver.findElement(By.cssSelector(".mk-search-trigger")).isDisplayed());
        } else {
            assert(driver.findElement(By.id("menu-item-link-get-started")).isDisplayed());
            assert(driver.findElement(By.id("menu-item-link-products")).isDisplayed());
            assert(driver.findElement(By.id("menu-item-link-resources")).isDisplayed());
            assert(driver.findElement(By.id("menu-item-link-about")).isDisplayed());
            assert(driver.findElement(By.id("menu-item-link-tips-&-stories")).isDisplayed());
            assert(driver.findElement(By.cssSelector(".mk-search-trigger")).isDisplayed());
            assertEquals(driver.getCurrentUrl(),url);

        }
    }
}