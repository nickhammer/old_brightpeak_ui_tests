import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class Assessments extends BaseTest {

    @Ignore
    public void testRegistrationPage() {
        driver.get("https://dh.dev.brightpeakfinancial.com/engage/account/register");
        WebElement loginLink = driver.findElement(By.linkText("Login"));
        loginLink.click();
        assertEquals(driver.getCurrentUrl(),"https://dh.dev.brightpeakfinancial.com/engage/account/register");
    }

    @Test
    public void testLogin(){
        driver.get("https://dh.dev.brightpeakfinancial.com/engage/account/login");
        WebElement emailTextField = driver.findElement(By.id("md-input-1"));
        emailTextField.sendKeys("nhammer105@mailinator.com");

        WebElement passwordTextField = driver.findElement(By.id("md-input-3"));
        passwordTextField.sendKeys("Bright1!");

        WebElement loginButton = driver.findElement(By.cssSelector(".mat-raised-button"));

        loginButton.click();

        pauseTest(10); // hard 10 second wait while page loads TODO: Replace this with a better version of a wait

        assert(driver.getCurrentUrl().contains("https://assessment.dev.brightpeakfinancial.com/index.php/survey/index"));
    }
}
